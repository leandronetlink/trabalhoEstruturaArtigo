import time
from Sort import Sort
import geradorDeEntradas

TAMANHO = 10000


sort = Sort(geradorDeEntradas.gerarQuaseOrdenado(TAMANHO))
# sort = Sort(geradorDeEntradas.gerarCompletamenteAleatorio(TAMANHO))
# sort = Sort(geradorDeEntradas.gerarQuaseInvertida(TAMANHO))

start_time = time.time()
sort.quickSort(0, TAMANHO - 1)


tempoExecucao = "{:.4} segundos".format(time.time() - start_time)

fileo = open("experimentoQuickSort.txt", "a")
# fileo.write("\nQUASE INVERTIDO |      {}    ".format(TAMANHO))
# fileo.write("\nCOMPLETAMENTE ALEATORIO |      {}    ".format(TAMANHO))
fileo.write("\nQUASE ORDENADO |      {}    ".format(TAMANHO))
fileo.write("    | {}".format(tempoExecucao))
fileo.write("    | {} atribuicoes".format(sort.getAtribuicoes()))
fileo.write("    | {} comparacoes".format(sort.getComparacoes()))
fileo.write("    | {} trocas".format(sort.getTrocas()))
fileo.write("    | {} recursoes".format(sort.getRecursoes()))
fileo.close()