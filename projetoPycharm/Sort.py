import sys
sys.setrecursionlimit(10000)


class Sort:
    A = dict()
    trocas = int()
    atribuicoes = int()
    comparacoes = int()
    recursoes = int()
    def __init__(self, lista):
        self.setList(lista)
        self.tamanho = len(self.A)
    #retorna array em uma string formatada
    def getArray(self):
        saida = ""
        for n in self.A:
            saida += str(self.A[n]) + " "
        return saida
    #recebe o array em string e armazena
    def setArray(self, string):
        self.A = dict()
        lista = string.split(' ')
        for n in range(len(lista)):
            self.A.setdefault(n, int(lista[n]))
    #ordena o array de entrada pelo metodo bubble sort
    def bubbleSort(self):
        trocado = True
        self.atribuicoes += 1
        while trocado:
            trocado = False
            self.atribuicoes += 1
            for j in range(self.tamanho - 1):
                self.comparacoes += 1
                if self.A[j] > self.A[j + 1]:
                    temp = self.A[j]
                    self.A[j] = self.A[j + 1]
                    self.A[j + 1] = temp
                    trocado = True
                    self.trocas += 1
                    self.atribuicoes += 4
    #ordena o array de entrada pelo metodo insertion sort
    def insertionSort(self):
        j = 1
        self.atribuicoes += 1
        for j in range(j, self.tamanho):
            temp = self.A[j]
            i = j - 1
            self.comparacoes += 1
            while i >= 0 and self.A[i] > temp:
                self.A[i + 1] = self.A[i]
                i -= 1
                self.atribuicoes += 2
            self.A[i + 1] = temp
            self.atribuicoes += 3
    #particiona o array de entrada para o uso no metodo quick sort
    def partition(self, p, r):
        x = self.A[r]
        i = p - 1
        j = p
        self.atribuicoes += 3
        for j in range(j, r):
            self.comparacoes += 1
            if self.A[j] <= x:
                i += 1
                temp = self.A[i]
                self.A[i] = self.A[j]
                self.A[j] = temp
                self.trocas += 1
                self.atribuicoes += 4
        temp = self.A[i + 1]
        self.A[i + 1] = self.A[r]
        self.A[r] = temp
        self.trocas += 1
        self.atribuicoes += 3
        return i + 1
    #ordena o array de entrada pelo metodo quick sort
    def quickSort(self, p, r):
        # self.comparacoes += 1
        if p < r:
            q = self.partition(p, r)
            self.atribuicoes += 1
            self.quickSort(p, q - 1)
            self.quickSort(q + 1, r)
            self.recursoes += 2
    #recebe lista de elementos
    def setList(self, lista):
        self.A = dict()
        for n in range(len(lista)):
            self.A.setdefault(n, int(lista[n]))
    def getTrocas(self):
        return self.trocas
    def getAtribuicoes(self):
        return self.atribuicoes
    def getComparacoes(self):
        return self.comparacoes
    def getRecursoes(self):
        return self.recursoes



