import time
from Sort import Sort
import geradorDeEntradas
TAMANHO = 10000


sort = Sort(geradorDeEntradas.gerarQuaseInvertida(TAMANHO))
# sort = Sort(geradorDeEntradas.gerarQuaseOrdenado(TAMANHO))
# sort = Sort(geradorDeEntradas.gerarCompletamenteAleatorio(TAMANHO))
start_time = time.time()

sort.insertionSort()


tempoExecucao = "{:.4} segundos".format(time.time() - start_time)
fileo = open("experimentoInsertionSort.txt", "a")
fileo.write("\nQUASE INVERTIDO |      {}    ".format(TAMANHO))
# fileo.write("\nCOMPLETAMENTE ALEATORIO |      {}    ".format(TAMANHO))
# fileo.write("\nQUASE ORDENADO |      {}    ".format(TAMANHO))
fileo.write("    | {}".format(tempoExecucao))
fileo.write("    | {} atribuicoes".format(sort.getAtribuicoes()))
fileo.write("    | {} comparacoes".format(sort.getComparacoes()))
fileo.write("    | N/A trocas".format(sort.getTrocas()))
fileo.write("    | N/A recursoes".format(sort.getRecursoes()))
fileo.close()
