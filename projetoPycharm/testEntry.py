import random
import time
from Sort import Sort


TAMANHO = 10000
lista = list()
for i in range(TAMANHO):
    if i <= int(TAMANHO * 0.10):
        lista.append(random.randint(1, int(TAMANHO * 0.10)))
    lista.append(i)

lista.reverse()
print(lista)

sort = Sort(lista)
#print(sort.getArray())
start_time = time.time()
sort.quickSort(0, TAMANHO)
#print(sort.getArray())
print(sort.getArray())


tempoExecucao = "Tempo de execução: {:.4} segundos".format(time.time() - start_time)

"""
le arquivo
file = open("testfile", "r")
string = file.read()
escreve arquivo
"""
fileo = open("saidaTempo", "w")
fileo.write("Qtd Numeros = {} ".format(TAMANHO))
fileo.write(tempoExecucao)
fileo.close()




