import random
def gerarCompletamenteAleatorio(TAMANHO):
    lista = list()
    for i in range(TAMANHO):
        lista.append(random.randint(1, TAMANHO))
    return lista

def gerarQuaseOrdenado(TAMANHO):
    lista = list()
    for i in range(TAMANHO):
        if i <= int(TAMANHO * 0.10):
            lista.append(random.randint(1, int(TAMANHO * 0.10)))
        lista.append(i)
    return lista


def gerarQuaseInvertida(TAMANHO):
    lista = list()
    for i in range(TAMANHO):
        if i <= int(TAMANHO * 0.10):
            lista.append(i)
            lista.reverse()
        lista.append(i)
    return lista